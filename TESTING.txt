
=Unit Tests

unit tests are located in the HuffmanTest sub-project

== EncoderTest.encodeTest()
input:	abbbcc
output:	byte[] expected = new byte[] {
				// table
				(byte)3, // elements in tables
				(byte)97, (byte)2, // byte value and bit count for 1st table entry
				(byte)99, (byte)2, // byte value and bit count for 2st table entry
				(byte)98, (byte)1, // byte value and bit count for 3st table entry
				(byte)24, // compressed table encoding bits
				// data
				(byte)7, // number of bits to discard from last byte
				(byte)58, (byte)128 // encoded data
			};

== DecoderTest.decodeTest()
input: <see output from EncoderTest.encodeTest() above>
output: <see input from EncoderTest.encodeTest() above>

== ProgramTest.MainTest()
input: text from resource file LoremIpsum (see HuffmanTest/Resources)
output: <same as input>
process: encode then decode text and make sure it matches
