﻿using Huffman;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace HuffmanTest
{
	 
	 
	 /// <summary>
	 ///This is a test class for EncoderTest and is intended
	 ///to contain all EncoderTest Unit Tests
	 ///</summary>
	[TestClass()]
	public class EncoderTest
	{
		#region HEADER

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		#endregion


		/// <summary>
		///A test for encode
		///</summary>
		[TestMethod()]
		public void encodeTest()
		{
			Huffman.Encoder target = new Huffman.Encoder();
			string text = "abbbcc";
			byte[] bytes = Encoding.ASCII.GetBytes(text);
			Stream input = new MemoryStream(bytes, false);
			Stream output = new MemoryStream();
			target.encode(input, output);

			verifyIndex(target);

			byte[] expected = new byte[] {
				// table
				(byte)3, // elements in tables
				(byte)97, (byte)2, // byte value and bit count for 1st table entry
				(byte)99, (byte)2, // byte value and bit count for 2st table entry
				(byte)98, (byte)1, // byte value and bit count for 3st table entry
				(byte)24, // compressed table encoding bits
				// data
				(byte)7, // number of bits to discard from last byte
				(byte)58, (byte)128 // encoded data
			};
			output.Position = 0;
			byte[] actual = new byte[output.Length];
			output.Read(actual, 0, (int)output.Length);
			CollectionAssert.AreEqual(expected, actual);

			//output.Position = 0;
			//StreamReader reader = new StreamReader(output);
			//Assert.AreEqual(text, reader.ReadToEnd());
		}

		private void verifyIndex(Huffman.Encoder target)
		{
			for (int i = 0; i < target.Length; i++)
			{
				Leaf<byte> leaf = target[i];
				switch (i)
				{
					case 97:
						Assert.AreEqual(i, leaf.Byte);
						Assert.AreEqual(1, leaf.Count);
						Assert.AreEqual(2, leaf.Encoding.Length);
						Assert.IsFalse(leaf.Encoding[0]);
						Assert.IsFalse(leaf.Encoding[1]);
						break;
					case 98:
						Assert.AreEqual(i, leaf.Byte);
						Assert.AreEqual(3, leaf.Count);
						Assert.AreEqual(1, leaf.Encoding.Length);
						Assert.IsTrue(leaf.Encoding[0]);
						break;
					case 99:
						Assert.AreEqual(i, leaf.Byte);
						Assert.AreEqual(2, leaf.Count);
						Assert.AreEqual(2, leaf.Encoding.Length);
						Assert.IsFalse(leaf.Encoding[0]);
						Assert.IsTrue(leaf.Encoding[1]);
						break;
					default:
						Assert.IsNull(target[i]);
						break;
				}
			}
		}


	}
}
