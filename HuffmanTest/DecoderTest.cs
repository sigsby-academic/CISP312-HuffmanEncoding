﻿using Huffman;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace HuffmanTest
{
	 
	 
	 /// <summary>
	 ///This is a test class for DecoderTest and is intended
	 ///to contain all DecoderTest Unit Tests
	 ///</summary>
	[TestClass()]
	public class DecoderTest
	{


		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion


		/// <summary>
		///A test for decode
		///</summary>
		[TestMethod()]
		public void decodeTest()
		{
			Decoder target = new Decoder();
			Stream input = new MemoryStream(new byte[] {
				// table
				(byte)3, // elements in tables
				(byte)97, (byte)2, // byte value and bit count for 1st table entry
				(byte)99, (byte)2, // byte value and bit count for 2snd table entry
				(byte)98, (byte)1, // byte value and bit count for 3srdtable entry
				(byte)24, // compressed table encoding bits
				// data
				(byte)7, // number of bits to discard from last byte
				(byte)58, (byte)128 // encoded data
			});
			Stream output = new MemoryStream();
			target.decode(input, output);
			//string expected = "abbbcc";
			//string actual = "";
			output.Position = 0;
			StreamReader reader = new StreamReader(output);
			Assert.AreEqual("abbbcc", reader.ReadToEnd());

		}
	}
}
