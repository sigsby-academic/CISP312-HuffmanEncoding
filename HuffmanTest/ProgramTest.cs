﻿using Huffman;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace HuffmanTest
{
	/// <summary>
	///This is a test class for ProgramTest and is intended
	///to contain all ProgramTest Unit Tests
	///</summary>
	[TestClass()]
	public class ProgramTest
	{


		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion


		/// <summary>
		///A test for Main
		///</summary>
		[TestMethod()]
		public void MainTest()
		{
			string text = Properties.Resources.LoremIpsum;

			Huffman.Encoder encoder = new Huffman.Encoder();
			byte[] bytes = Encoding.ASCII.GetBytes(text);
			Stream enIn = new MemoryStream(bytes, false);
			Stream enOut = new MemoryStream();
			encoder.encode(enIn, enOut);
			enOut.Position = 0;

			Huffman.Decoder decoder = new Huffman.Decoder();
			Stream deIn = enOut;
			Stream deOut = new MemoryStream();
			decoder.decode(deIn, deOut);
			deOut.Position = 0;
			StreamReader reader = new StreamReader(deOut);

			string actual = reader.ReadToEnd();
			Assert.AreEqual(text, actual);

		}
	}
}
