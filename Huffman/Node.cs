﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Huffman
{
	abstract class Node : IComparable<Node>
	{
		protected int count = 0;
		public int Count { get { return count; } }

		protected Branch parent;
		public Branch Parent
		{
			get { return parent; }
			set { parent = value; }
		}

		public int CompareTo(Node n)
		{
			if(Object.ReferenceEquals(this,n)) return 0;
			else return this.Count > n.Count ? 1 : -1;
		}

		public virtual BitArray Encoding
		{
			get
			{
				BitArray bits;
				if(parent == null)
				{
					bits = new BitArray(0);
				}
				else
				{
					bits = parent.Encoding;
					bits.Length++;
					bits[bits.Length - 1] = (parent[1] == this);
				}
				return bits;
			}
		}

	}
}
