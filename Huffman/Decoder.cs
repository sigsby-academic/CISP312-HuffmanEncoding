﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Huffman
{
	class Decoder
	{
		private Stream input;
		public Stream Input
		{
			get { return input; }
			private set
			{
				if (!value.CanRead)
					throw new Exception("input stream must support reading");
				input = value;
			}
		}

		private Stream output;
		public Stream Output
		{
			get { return output; }
			private set
			{
				if (!value.CanWrite)
					throw new Exception("output stream must support writing");
				output = value;
			}
		}

		public void decode(Stream input, Stream output)
		{
			Input = input;
			Output = output;
			readTable();
			decodeData();
		}

		Dictionary<string, byte> table = new Dictionary<string, byte>();

		public void readTable()
		{
			// table length
			byte length = (byte)input.ReadByte();

			// get all table bytes values and bit counts
			byte[] header = new byte[length * 2];
			input.Read(header, 0, header.Length);

			// iterate header and get encoding bits
			int bitPointer = 7;
			BitArray currentByte = new BitArray(new byte[] { (byte)(input.ReadByte()) });
			for (byte i = 0; i < header.Length; i++)
			{
				byte value = header[i++];
				string encoding = "";
				for (byte j = header[i]; j > 0; j--)
				{
					if (bitPointer < 0)
					{
						bitPointer = 7;
						currentByte = new BitArray(new byte[] { (byte)(input.ReadByte()) });
					}
					encoding += (currentByte[bitPointer--] ? "1" : "0");
				}
				table.Add(encoding, value);
			}
		}

		public void decodeData()
		{
			byte discard = (byte)input.ReadByte();
			string encoding = "";

			//for(byte b = (byte)input.ReadByte(); b != -1; byte b = (byte)input.ReadByte())
			while(input.Position < input.Length)
			{
				BitArray b = new BitArray(new byte[] { (byte)input.ReadByte() } );
				for (int i = 7; i >= 0; i--)
				{
					
					encoding += b[i] ? "1" : "0";
					if (table.ContainsKey(encoding))
					{
						output.WriteByte(table[encoding]);
						encoding = "";
					}
					if(input.Position == input.Length && i == discard) break;
				}
			}

		}

	}
}
