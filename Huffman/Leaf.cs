﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Huffman
{
	class Leaf<T> : Node
	{
		// fields
		private T b;
		private BitArray encoding;

		// properties
		public T Byte { get { return b; } }

		public override BitArray Encoding
		{
			get
			{
				if(encoding == null) encoding = base.Encoding;
				return encoding;
			}
		}


		// constructors
		public Leaf(T b) { this.b = b; }
		public Leaf(T b, int count) { this.b = b; base.count = count; }

		// methods
		public void Increment() { count++; }

		public static Leaf<T> operator ++(Leaf<T> n) { n.count++; return n; }

	}
}
