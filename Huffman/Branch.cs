﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Huffman
{
	class Branch : Node
	{
		
		private Node[] child = new Node[2];
		
		public Node this[int i] { get { return child[i]; } }

		public Branch(Node a, Node b)
		{
			if (a.Count < b.Count) { child[0] = a; child[1] = b; }
			else { child[0] = b; child[1] = a; }
			child[0].Parent = this;
			child[1].Parent = this;
			count = child[0].Count + child[1].Count;
		}

	}
}
