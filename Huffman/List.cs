﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Huffman
{
	class List<T> : IEnumerable<T>
	{
		private System.Collections.Generic.List<T> list
			= new System.Collections.Generic.List<T>();

		public int Count {
			get {
				return list.Count;
			}
		}
		
		public T this[int i] {
			get {
				return list[i];
			}
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach(T item in list) yield return item;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// insert the item in sorted order
		// if not in list, returns the binary complement
		// if the index where item should be inserted
		// does nothing if item is already in list
		public void Insert(T item)
		{
			int i = list.BinarySearch(item);
			if (i < 0) list.Insert(~i, item);
		}

		// remove first item in list
		// and return it
		public T Pop()
		{
			T item = list[0];
			list.RemoveAt(0);
			return item;
		}

	}
}
