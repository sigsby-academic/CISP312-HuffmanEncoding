﻿using System;
using System.IO;
using System.Collections;

namespace Huffman
{
	// TODO: refactor, writeTable() and writeData() use identical code 
	// to write a BitArray as bytes to a Stream 

	class Encoder
	{
		private Stream input;
		public Stream Input
		{
			get { return input; }
			private set {
				if(!value.CanRead)
					throw new Exception("input stream must support reading");
				input = value;
			}
		}

		private Stream output;
		public Stream Output
		{
			get { return output; }
			private set {
				if(!value.CanWrite)
					throw new Exception("output stream must support writing");
				output = value;
			}
		}

		//private Stream data;
		//public Stream Data
		//{
		//   get { return data; }
		//   private set {
		//      if(!(value.CanWrite && value.CanRead))
		//         throw new Exception("output stream must support writing");
		//      data = value;
		//   }
		//}

		private Leaf<byte>[] index = new Leaf<byte>[256];
		private Leaf<byte>[] Index { set { index = value; } }
		public Leaf<byte> this[int i] { get { return index[i]; } }
		public int Length { get { return index.Length; } }

		// stores the full sorted list of leaf nodes
		List<Leaf<byte>> list = new List<Leaf<byte>>();

		// root of binary search tree
		private Branch tree;

		public void encode(Stream input, Stream output)
		{
			Input = input;
			Output = output;
			buildIndex();
			buildTree();
			writeTable();
			writeData();
		}

		// count byte frequencies
		// stores bytes from first pass into data stream
		// for second pass to encode
		private void buildIndex()
		{
			// initialize properties
			//Data = new MemoryStream();
			Index = new Leaf<byte>[256];

			// read each byte from input
			byte b;
			//while (data.Length < input.Length)
			while (input.Position < input.Length)
			{
				b = (byte)input.ReadByte();
				// add byte to data stream for encoding (2nd pass) later
				//data.WriteByte(b);

				// init/update counts
				if(index[b] == null) index[b] = new Leaf<byte>(b);
				index[b]++;
			}
		}

		// build initial sorted list from count
		// in ascending frequency
		// then build tree
		private void buildTree()
		{
			List<Node> t = new List<Node>();
			// insert items from index into sorted list
			foreach(Leaf<byte> i in index) if(i != null)
			{
				list.Insert(i);
				t.Insert(i);
			}
			// compress list into single root node
			// each List.Insert() keeps list sorted
			while(t.Count > 1) t.Insert(new Branch(t.Pop(), t.Pop()));
			tree = (Branch)t.Pop();
		}

		// encoding table is also compressed
		// 1st byte = number of unique bytes
		// for each unique byte
		//		1st byte = byte value (unencoded)
		//		2nd byte = number of bits in encoding
		// n bytes, where n = sum of bits in each encoding (rounded up)
		private void writeTable()
		{
			// write table size
			output.WriteByte((byte)list.Count);
			// store encoding bits for later
			BitArray bits = new BitArray(0);
			// write each entry
			foreach (Leaf<byte> item in list)
			{
				// write actual byte value
				output.WriteByte(item.Byte);

				// write encoding
				BitArray encoding = item.Encoding;

				// write number of bits in encoding
				output.WriteByte((byte)encoding.Count);

				// add item encoding to master bitarray of encoding bits
				for (int i = 0; i < encoding.Count; i++)
				{
					bits.Length = bits.Length +1;
					bits[bits.Length - 1] = encoding[i];
				}
			}
			// write final encoding table bits
			byte b = 0;
			for (int i = 0; i < bits.Count; i++)
			{
				// set correct bit using OR
				if (bits[i]) b |= (byte)(1 << (7 - (i % 8)));
				/// if byte is full (or very last bit), write it and clear
				if ((((i + 1) % 8) == 0) || ((i + 1) == bits.Count))
				{
					output.WriteByte(b);
					b = 0;
				}
			}
		}
		
		// write the encoded value starting with the
		// number of bits to discard from the last byte
		private void writeData()
		{
			// used to build encoded value as BitArray
			BitArray bits = new BitArray(0);
			byte b = 0;
			// read each byte data stream
			// make sure data stream is reset to beginning
			//data.Position = 0;
			input.Position = 0;

			//for(int i = 0; i < data.Length; i++)
			for(int i = 0; i < input.Length; i++)
			{
				//b = (byte)data.ReadByte();
				b = (byte)input.ReadByte();

				// get byte encoding and add to BitArray
				BitArray encoding = index[b].Encoding;
				foreach (bool eb in encoding)
				{
					bits.Length = bits.Count + 1;
					bits[bits.Count - 1] = eb;
				}
			}

			// write number of bits to discard from last byte
			output.WriteByte((byte)(8 - (bits.Count % 8)));

			// write final encoding table bits
			b = 0;
			for (int i = 0; i < bits.Count; i++)
			{
				// set correct bit using OR
				if (bits[i]) b |= (byte)(1 << (7 - (i % 8)));
				/// if byte is full (or very last bit), write it and clear
				if ((((i + 1) % 8) == 0) || ((i + 1) == bits.Count))
				{
					output.WriteByte(b);
					b = 0;
				}
			}
		}


	}
}
