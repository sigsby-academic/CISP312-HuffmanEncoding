﻿using System;
using System.IO;
using System.Diagnostics;

namespace Huffman
{
	class Program
	{
		public const string ENCODE = "encode";
		public const string DECODE = "decode";

		private static int status = 0;
		
		private static TextWriter error = Console.Error;
		private static FileStream input;
		private static FileStream output;

		private static Stopwatch timer = new Stopwatch();

		static void usage()
		{
			error.WriteLine("Usage: Huffman <"+ENCODE+"|"+DECODE+"> <input file> <output file>");
		}

		static int Main(string[] args) {
		// check for correct number of arguments
			if (args.Length < 3 || (args.Length % 3) != 0) { status = 1; }
			else try
			{
				// loop through cli arguments by threes
				// this allows for multiple encode/decode operations
				// e.g. Huffman encode file.txt file.encoded decode file.encoded file.decoded
				for (int i = 0; i < args.Length; i++)
				{
					string op = args[i];
					string infile = args[++i];
					string outfile = args[++i];

					Console.WriteLine("Operation {0}: {1} {2} {3} ",(i/3),op,infile,outfile);

					// open filestreams
					input = new FileStream(infile, FileMode.Open);
					output = new FileStream(outfile, FileMode.Create);

					// performance profiling
					timer.Restart();

					processMemoryStream(op);
					//processFileStream(op);

					// performance profiling
					timer.Stop();

					// close file streams
					input.Dispose();
					output.Dispose();

					// performance profiling
					Console.WriteLine("Operation {0}: elapsed time {1}", (i / 3), timer.Elapsed.ToString());
					timer.Reset();

				} // end cli argument set loop
			}
				catch (Exception ex)
				{
					#if DEBUG
					error.WriteLine(ex.ToString());
					#endif
					error.WriteLine(ex.Message);
					status = 1;
				}
			finally
			{
				// make sure we don't leave any streams open
				if(input != null) input.Dispose();
				if(output != null) output.Dispose();

				// wrap up profiling in case of exception
				timer.Stop();
			}
			if(status != 0) usage();
			Console.WriteLine("Press any key to exit...");
			Console.ReadKey(true);
			return status;
		}

		private static void processFileStream(string op)
		{
			process(op, input, output);
		}

		private static void processMemoryStream(string op)
		{
			// init memory streams
			MemoryStream instream = new MemoryStream();
			MemoryStream outstream = new MemoryStream();
			input.CopyTo(instream);
			instream.Position = 0;

			// process
			process(op, instream, outstream);

			// copy temp memory stream to final output stream
			outstream.Position = 0;
			outstream.CopyTo(output);

			// close memory streams
			instream.Dispose();
			outstream.Dispose();
		}

		private static void process(string op, Stream instream, Stream outstream)
		{
			switch (op)
			{
				case ENCODE:
					Encoder encoder = new Encoder();
					//encoder.encode(input, output);
					encoder.encode(instream, outstream);
					break;
				case DECODE:
					Decoder decoder = new Decoder();
					//decoder.decode(input, output);
					decoder.decode(instream, outstream);
					break;
				default:
					throw new Exception("Operation not supported");
			}
		}


	}
}
